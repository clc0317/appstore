class App < ActiveRecord::Base
	belongs_to :user  # NOTE: let's do this for now

	has_many :app_files
	has_many :reviews

  validates :bundle_identifier_suffix, format: { with: /\A[a-zA-Z\.]+\Z/, message: "only allow dots and letters" }

  has_attached_file :icon, styles: { itunes: ["512x512#", :png], ios_download: ["57x57#", :png] }, default_url: lambda { |image| ActionController::Base.helpers.asset_path('appstore-icon.png') }
  validates_attachment :icon, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }


  def ios_application_identifier
    # i made a mistake to use '_' in bundle identifiers
    # this is fixed on 2015-12-1.  This check is added for backward compatibility
    if created_at < DateTime.new(2015, 12, 1, 21)
      return 'edu.duke.colab.' + self.id.to_s + '_' + self.bundle_identifier_suffix
    end

    'edu.duke.colab.' + self.id.to_s + '-' + self.bundle_identifier_suffix
  end

  def author_display_name
    team_name || try(:user).try(:display_name)
  end

end
