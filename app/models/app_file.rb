class AppFile < ActiveRecord::Base
  belongs_to :app
  belongs_to :user

  validates :app, presence: true
  validates_inclusion_of :platform, :in => %w( Android iOS )
  validates :version, presence: true

  has_attached_file :archive
  validates_attachment_file_name :archive, matches: [/apk\Z/, /zip\Z/]
  validates_with AttachmentPresenceValidator, attributes: :archive, :on => :create
  validates_with AttachmentSizeValidator, attributes: :archive, less_than: 300.megabytes

  enum code_signing_status: { unsigned: 0, downloading_to_mac: 4, signing: 1, signing_failed: 2, pending_transfer_to_web: 5, downloading_to_web: 6, download_to_web_failed: 7, signed: 3 }
  after_commit :queue_for_ios_code_signing, :on => :create

  def signed_ipa_location
    File.join(File.dirname(archive.path), 'Signed.ipa')
  end

  def queue_for_ios_code_signing
    puts "code_signing_status = " + code_signing_status
    puts "platform = " + platform
    puts "id = " + id.to_s
    if is_ios_app?
      # make a secret random key for direct download
      require 'securerandom'
      update_column(:download_token, SecureRandom.hex)

      BuildIpaJob.perform_later id
    end
  end

  def is_ios_app?
    platform == "iOS"
  end

  def is_android_app?
    platform == "Android"
  end

  def itms_services_url
    unless is_ios_app?
      raise "Only iOS apps have itms-services URLs"
    end

    url_options = Rails.application.config.action_mailer.default_url_options
    url_options[:format] = :plist
    url_options[:protocol] = :https
    'itms-services://?action=download-manifest&url=' + Rails.application.routes.url_helpers.app_app_file_download_url(app, self, url_options)
  end

  def apk_url
    unless is_android_app?
      raise "Only Android apps have apk_url"
    end

    url_options = Rails.application.config.action_mailer.default_url_options
    url_options[:format] = :apk
    url_options[:protocol] = :https
    Rails.application.routes.url_helpers.app_app_file_download_url(app, self, url_options)
  end

end
