class User < ActiveRecord::Base
	has_and_belongs_to_many :apps
	has_many :reviews

  # NOTE: caller must ensure user is authenticated
  def self.find_or_create_by_netid(netid)
    user = where(:netid => netid).first || create_by_netid(netid)
    user.update_info_from_duke_api!
    user
  end

  def self.find_or_create_by_netid_pass(netid, pass)
    if !auth_by_netid_pass(netid, pass)
      return nil
    end

    user = where(:netid => netid).first || create_by_netid(netid)
    user.update_info_from_duke_api!

    user
  end

  def self.create_by_netid(netid)
    create!(netid: netid)
  end

  def self.auth_by_netid_pass(netid, pass)
    logged_in = false
    begin
      Net::SSH.start(
        'teer23.oit.duke.edu', netid,
        :password => pass, 
        :auth_methods => ['password'],
        :number_of_password_prompts => 0
      ) do |ssh|
        logged_in = true
      end
    rescue Net::SSH::AuthenticationFailed
    end

    return logged_in
  end

  def update_info_from_duke_api!
    require 'open-uri'
    require 'cgi'
    # TODO: rewrite as soon as Identity API (and the gem) become available
    api_response = JSON.load(open('https://streamer.oit.duke.edu/ldap/people/netid/' + CGI.escape(netid) + '?access_token=' + Settings.streamer_api.api_key))[0]
    write_attribute(:display_name, api_response['display_name'])
    save!
  end
end
