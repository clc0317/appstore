json.array!(@app_files) do |app_file|
  json.extract! app_file, :id, :platform, :version, :downloadCount, :file, :app_id
  json.url app_file_url(app_file, format: :json)
end
