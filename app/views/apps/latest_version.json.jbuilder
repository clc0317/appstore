unless @latest_ios.nil?
  json.ios do
    json.(@latest_ios, :version, :itms_services_url)
  end
end

unless @latest_android.nil?
  json.android do
   json.(@latest_android, :version, :archive_url)
  end
end
