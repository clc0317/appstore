class SessionsController < ApplicationController
  def create_during_test
    if Rails.env.test? || Rails.env.development?
      session[:user_id] = params[:user_id]
    end
    redirect_to :root
  end

  def create
    user = User.find_or_create_by_netid_pass(params[:netid], params[:password])
    if user == nil
      return redirect_to sessions_signin_path, alert: 'Incorrect NetID or password.'
    end
    session[:user_id] = user.id
    redirect_to :root, notice: 'Welcome!'
  end

  def signin
  end

  def destroy
    session[:user_id] = nil
    redirect_to :root, notice: 'You are logged out.'
  end

  def oauth_create
    # An OmniAuth AushHash looks like this:
    # --- !ruby/hash:OmniAuth::AuthHash
    # provider: duke_oauth2
    # uid: jz143@duke.edu
    # info: !ruby/hash:OmniAuth::AuthHash::InfoHash
    #   email: jz143@duke.edu
    #   netid: jz143
    # credentials: !ruby/hash:OmniAuth::AuthHash
    #   token: ...
    #   refresh_token: ...
    #   expires_at: 1457386355
    #   expires: true
    # extra: !ruby/hash:OmniAuth::AuthHash
    #   raw_info: !ruby/hash:OmniAuth::AuthHash
    #     eppn: jz143@duke.edu
    #     scope: basic

    user = User.find_or_create_by_netid(auth_hash.info.netid)
    session[:user_id] = user.id
    redirect_to(request.env['omniauth.origin'] || :root, notice: 'Welcome!')
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
