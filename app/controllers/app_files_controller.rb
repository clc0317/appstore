class AppFilesController < ApplicationController
  before_action :set_app_file, only: [:show, :edit, :update, :destroy]

  # GET /app_files
  # GET /app_files.json
  def index
    @app_files = AppFile.where(app_id: params[:app_id])
  end

  # GET /app_files/1
  # GET /app_files/1.json
  def show
    authorize @app_file
  end

  # GET /app_files/1/download
  def download
    @app_file = AppFile.find(params[:app_file_id])
    respond_to do |format|
      format.zip do
        if params[:download_token] == @app_file.download_token
          send_file @app_file.archive.path
        else
          head :bad_request
        end
      end

      format.apk do
        send_file @app_file.archive.path
        increment_download_count
      end

      format.plist do
        if @app_file.signed?
          render 'manifest.plist'
        else
          render json: { :error => 'Signed ipa is not available yet' }, status: :bad_request
        end
        increment_download_count
      end

      format.ipa do
        if @app_file.signed?
          send_file @app_file.signed_ipa_location, :type => 'application/octet-stream'
        else
          render json: { :error => 'Signed ipa is not available yet' }, status: :bad_request
        end
      end
    end
  end

  def build_log
    @app_file = AppFile.find(params[:app_file_id])
    authorize @app_file, :edit?
    render plain: @app_file.code_signing_log
  end

  # GET /app_files/new
  def new
    @app_file = AppFile.new
    @app_file.app_id = params[:app_id]
    authorize @app_file
  end

  # GET /app_files/1/edit
  def edit
    authorize @app_file
  end

  # POST /app_files
  # POST /app_files.json
  def create
    @app_file = AppFile.new(app_file_params)
    @app_file.app_id = params[:app_id]

    authorize @app_file

    respond_to do |format|
      if @app_file.save
        format.html { redirect_to @app_file.app, notice: 'App file has been uploaded.' }
        format.json { render :show, status: :created, location: @app_file }

        SendSlackUpdateJob.perform_later "[!!] %s uploaded [%s](%s) (ver. %s for %s) <!channel>" \
          % [if current_user.nil? then 'Guest' else current_user.display_name end, @app_file.app.name, app_url(@app_file.app), @app_file.version, @app_file.platform].map { |s| SlackNotifier.escape(s.to_s) }
      else
        format.html { render :new }
        format.json { render json: @app_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /app_files/1
  # PATCH/PUT /app_files/1.json
  def update
    authorize @app_file
    respond_to do |format|
      if @app_file.update(app_file_params)
        format.html { redirect_to @app_file, notice: 'App file was successfully updated.' }
        format.json { render :show, status: :ok, location: @app_file }
      else
        format.html { render :edit }
        format.json { render json: @app_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /app_files/1
  # DELETE /app_files/1.json
  def destroy
    authorize @app_file
    @app_file.destroy
    respond_to do |format|
      format.html { redirect_to app_app_files_url(@app_file.app), notice: 'App file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def increment_download_count
      # ignore HEAD requests
      if request.head?
        return
      end

      unless request.headers['Range'].nil?
        # ignore request if Range header exists and starting range is not 0
        range = request.headers['Range'].match(/bytes=(?<begin>\d+)-(?<end>\d*)/)
        if range[:begin] != '0'
          return
        end
      end
      
      @app_file.increment!(:download_count)

      SendSlackUpdateJob.perform_later "%s (IP: %s) downloaded [%s](%s) (ver. %s for %s)" \
        % [if current_user.nil? then 'Guest' else current_user.display_name end, request.remote_ip, @app_file.app.name, app_url(@app_file.app), @app_file.version, @app_file.platform].map { |s| SlackNotifier.escape(s.to_s) }
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_app_file
      @app_file = AppFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def app_file_params
      params.require(:app_file).permit(:platform, :version, :archive, :app_id)
    end
end
