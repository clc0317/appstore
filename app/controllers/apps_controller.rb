class AppsController < ApplicationController
  before_action :set_app, only: [:show, :edit, :update, :destroy, :latest_version]

  # GET /apps
  # GET /apps.json
  def index
    @apps = App.all
  end

  # GET /apps/1
  # GET /apps/1.json
  def show
    authorize @app
    set_latest_versions
  end

  # public API
  # do not check authorization!
  def latest_version
    set_latest_versions
  end

  # GET /apps/new
  def new
    @app = App.new
    authorize @app
  end

  # GET /apps/1/edit
  def edit
    authorize @app
  end

  # POST /apps
  # POST /apps.json
  def create
    @app = App.new(app_params)
    @app.user = current_user

    authorize @app

    respond_to do |format|
      if @app.save
        format.html { redirect_to @app, notice: 'App was successfully created.' }
        format.json { render :show, status: :created, location: @app }

        SendSlackUpdateJob.perform_later "[!!!] %s created a new app [%s](%s) <!channel>" \
          % [if current_user.nil? then 'Guest' else current_user.display_name end, @app.name, app_url(@app)].map { |s| SlackNotifier.escape(s.to_s) }
      else
        format.html { render :new }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /apps/1
  # PATCH/PUT /apps/1.json
  def update
    authorize @app
    respond_to do |format|
      if @app.update(app_params)
        format.html { redirect_to @app, notice: 'App was successfully updated.' }
        format.json { render :show, status: :ok, location: @app }

        SendSlackUpdateJob.perform_later "[!!!] %s updated information of app [%s](%s) <!channel>" \
          % [if current_user.nil? then 'Guest' else current_user.display_name end, @app.name, app_url(@app)].map { |s| SlackNotifier.escape(s.to_s) }
      else
        format.html { render :edit }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apps/1
  # DELETE /apps/1.json
  def destroy
    authorize @app
    @app.destroy
    respond_to do |format|
      format.html { redirect_to apps_url, notice: 'App was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_app
      @app = App.find(params[:id])
    end

    # sets @latest_ios and @latest_android to latest app_file's for each platform
    def set_latest_versions
      @latest_ios = @app.app_files.where(platform: 'iOS', code_signing_status: AppFile.code_signing_statuses[:signed]).order(created_at: :desc).first
      @latest_android = @app.app_files.where(:platform => 'Android').order(created_at: :desc).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def app_params
      params.require(:app).permit(:name, :description, :icon, :bundle_identifier_suffix)
    end
end
