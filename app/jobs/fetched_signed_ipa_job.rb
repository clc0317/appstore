class FetchedSignedIpaJob < ActiveJob::Base
  queue_as :web_background

  def perform(app_file_id)
    ActiveRecord::Base.clear_active_connections!

    # get corresponding app_file record
    app_file = AppFile.find(app_file_id)

    log = app_file.code_signing_log
    begin
      app_file.downloading_to_web!

      # get file to app_file.signed_ipa_location
      log << "scp from #{Settings.signing_worker.user}@#{Settings.signing_worker.host}:#{app_file.signed_ipa_path_on_signing_server} to #{app_file.signed_ipa_location}\n"
      Net::SCP.download!(Settings.signing_worker.host, Settings.signing_worker.user,
        app_file.signed_ipa_path_on_signing_server, app_file.signed_ipa_location)

      # remove remote tmpdir and file
      log << "removing sign:#{File.dirname(app_file.signed_ipa_path_on_signing_server)}\n"
      Net::SSH.start(Settings.signing_worker.host, Settings.signing_worker.user) do |ssh|
        ssh.exec "rm -rf \"#{File.dirname(app_file.signed_ipa_path_on_signing_server)}\""
      end

      # remove user uploaded unsigned package to save space
      log << "removing local, unsigned copy\n"
      FileUtils.remove_file(app_file.archive.path, true)

      app_file.signed!
    rescue => e
      app_file.download_to_web_failed!
      log << "Fatal error: " << e.to_s << "\n"
    end

    # save log and states to database without validation and callbacks
    app_file.update_column(:code_signing_log, log)
  end
end
