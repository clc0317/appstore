class SendSlackUpdateJob < ActiveJob::Base
  queue_as :web_background

  def perform(message, attachments=[])
    SlackNotifier.ping message, attachments: attachments
  end
end
