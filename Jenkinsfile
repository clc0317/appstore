pipeline {
  agent {
    node {
      label 'atomic'
    }
  }
  stages {
    stage ('Generate Dockerfile') {
      steps {
        sh '''
             source /build/pipeline_vars
             repackage_upstream_image stevedore-repo.oit.duke.edu/el7-rails
           '''
      }
    }
    stage ('Build Image') {
      steps {
        sh '''
             source /build/pipeline_vars
             build
           '''
      }
    }
    stage ('Save Build Artifact') {
      steps {
        sh '''
            source /build/pipeline_vars
            docker tag ${BUILD_IMAGE_TAG} ${REMOTE_IMAGE_TAG}
            docker push ${REMOTE_IMAGE_TAG}
           '''
      }
    }
    stage('Test') {
      steps {
        parallel (
          Integrity: {
            sh '''
              source /build/pipeline_vars
              test
            '''
          },
          SysSecurity: {
            sh '''
                source /build/pipeline_vars
                check_cves 7.0
            '''
          }
        )
      }
    }
    stage('Push Image Latest') {
      steps {
        sh '''
           source /build/pipeline_vars
           tag
           '''
      }
    }
  }
  post {
      always {
          sh '''
             source /build/pipeline_vars
             cleanup_images
             '''
      }
      failure {
        mail(from: "doozer@build.docker.oit.duke.edu",
             to: "christopher.collins@duke.edu" "ml354@duke.edu",
             subject: "${JOB_NAME} Image Build Failed",
             body: "Something failed with the ${JOB_NAME} image build: ${RUN_DISPLAY_URL}")
      }
  }
}
