role :app, %w{appstore@appstore.colab.duke.edu}
role :web, %w{appstore@appstore.colab.duke.edu}
role :db, %w{appstore@appstore.colab.duke.edu}

set :rvm_ruby_version, '2.2.1'
