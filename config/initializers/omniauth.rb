Rails.application.config.middleware.use OmniAuth::Builder do
  provider :duke_oauth2, Settings.duke_oauth2.client_id, Settings.duke_oauth2.client_secret
end
