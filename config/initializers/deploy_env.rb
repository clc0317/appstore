class DeployEnv
  @@git_commit = `git rev-parse --short HEAD 2>/dev/null`.strip

  def self.git_commit
    @@git_commit = @@git_commit.presence || File.read('REVISION').strip
  end
end
