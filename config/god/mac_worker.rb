God.watch do |w|
  w.name = "mac_worker"

  unless ENV['APPSTORE_RAILS_ROOT'].nil?
    w.dir = ENV['APPSTORE_RAILS_ROOT']
  else
    w.dir = '/Users/appstore/appstore_sign'
  end

  w.env = { 'VERBOSE' => '1',
            'QUEUE' => 'mac_online' }
  w.start = "bash -l -c 'rake environment resque:work'"
  w.keepalive
end
