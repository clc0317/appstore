God.watch do |w|
  w.name = "web_worker"
  
  unless ENV['APPSTORE_RAILS_ROOT'].nil?
    w.dir = ENV['APPSTORE_RAILS_ROOT']
  else
    w.dir = '/home/appstore/appstore/current'
  end

  w.env = { 'VERBOSE' => '1',
            'QUEUE' => 'web_background' }
  w.start = "bash -l -c 'rake environment resque:work'"
  w.keepalive
end
