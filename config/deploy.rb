# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'appstore'
set :repo_url, 'git@gitlab.oit.duke.edu:colab/appstore.git'
set :branch, 'deploy'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/appstore/appstore'

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/settings.local.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'resources')

# Default value for default_env is {}
set :default_env, { path: "/usr/local/bin/:$PATH" }

namespace :deploy do

  desc 'Upload secret configuration files'
  task :upload_config do
    on roles(:app), in: :sequence do
      # secrets
      execute :mkdir, '-p', "#{shared_path}/config"
      upload! 'config/database.yml', "#{shared_path}/config/database.yml"
      upload! 'config/secrets.yml', "#{shared_path}/config/secrets.yml"
      upload! 'config/settings.local.yml', "#{shared_path}/config/settings.local.yml"
    end

    # TODO: automate configuring signing server
  end

  desc 'Restart services'
  task :restart do
    on roles(:app) do
      invoke 'puma:restart'
    end
  end

  desc 'Post-deploy messages'
  task :post_deploy_msgs do
    STDERR.puts "="*80
    STDERR.puts "MANUAL STEPS AFTER DEPLOY"
    STDERR.puts "="*80
    STDERR.puts "Restart Resque worker as root: systemctl restart appstore_worker"
    STDERR.puts "="*80
  end

  before 'deploy:check:linked_dirs', :upload_config
  after 'deploy:publishing', :restart
  after 'deploy:log_revision', :post_deploy_msgs

end
