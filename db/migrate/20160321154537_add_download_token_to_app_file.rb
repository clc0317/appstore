class AddDownloadTokenToAppFile < ActiveRecord::Migration
  def change
    add_column :app_files, :download_token, :string
    add_column :app_files, :signed_ipa_path_on_signing_server, :string
  end
end
