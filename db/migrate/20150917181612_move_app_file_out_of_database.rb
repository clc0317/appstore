class MoveAppFileOutOfDatabase < ActiveRecord::Migration
  def change
    remove_column :app_files, :file
    add_attachment :app_files, :archive
  end
end
