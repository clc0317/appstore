class AddTeamNameToApps < ActiveRecord::Migration
  def change
    add_column :apps, :team_name, :string
  end
end
