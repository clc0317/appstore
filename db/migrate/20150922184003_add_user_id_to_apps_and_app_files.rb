class AddUserIdToAppsAndAppFiles < ActiveRecord::Migration
  def change
    add_column :apps, :user_id, :integer
    add_column :app_files, :user_id, :integer
  end
end
