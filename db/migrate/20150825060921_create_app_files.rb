class CreateAppFiles < ActiveRecord::Migration
  def change
    create_table :app_files do |t|
      t.string :platform
      t.string :version
      t.string :downloadCount
      t.binary :file
      t.belongs_to :app, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
