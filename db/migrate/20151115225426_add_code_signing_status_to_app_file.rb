class AddCodeSigningStatusToAppFile < ActiveRecord::Migration
  def change
    add_column :app_files, :code_signing_status, :integer, default: 0
  end
end
