class ChangeLogTypeToLongtext < ActiveRecord::Migration
  def change
    change_column :app_files, :code_signing_log, :text, :limit => 4294967295
  end
end
