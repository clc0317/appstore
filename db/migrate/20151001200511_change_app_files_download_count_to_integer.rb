class ChangeAppFilesDownloadCountToInteger < ActiveRecord::Migration
  def change
    change_column :app_files, :download_count, :integer, default: 0
  end
end
