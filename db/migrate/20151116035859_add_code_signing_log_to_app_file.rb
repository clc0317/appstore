class AddCodeSigningLogToAppFile < ActiveRecord::Migration
  def change
    add_column :app_files, :code_signing_log, :text
  end
end
