source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use sqlite3 as the database for Active Record
gem 'sqlite3', group: [:development, :test]
gem 'mysql2', '~> 0.3.18', group: :production

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby, group: :production

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

gem 'puma'

# Use Capistrano for deployment
group :development do
  gem 'capistrano-rvm'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano3-puma'
end

# Use database for session store
gem 'activerecord-session_store'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

# use config to store API secrets, etc.
gem "config", "~> 1.0.0"

# store user uploaded app package files
gem "paperclip", "~> 4.3"
gem 'simple_form'

# authentication
# gem 'devise', "~> 3.5.2"
gem 'net-ssh', '~> 3.1.0'
gem 'net-scp', '~> 1.2.1'

# authorization
gem "pundit"

# generate plist on the fly
gem "plist", :git => "https://github.com/jiehanzheng/plist.git", :ref => "8dc72ad"

gem "omniauth", '~> 1.3.1'
gem "omniauth-duke-oauth2", :git => 'http://gitlab.oit.duke.edu/colab/omniauth-duke-oauth2.git', :ref => '5eaf6759'

# job queue
gem 'resque', '~> 1.26.0'

# process monitor for background workers, etc.
gem 'god', '~> 0.13.7'

gem 'bootstrap-sass', '~> 3.3.5'

# post to Slack
gem 'slack-notifier', '~> 1.5.1'
